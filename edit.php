<?php
// including the database connection file
include ("config.php");

if(isset($_POST['update']))
{	
	$id = $_POST['id'];
	$user = array (
				'nombre' => $_POST['nombre'],
				'edad' => $_POST['edad'],
				'email' => $_POST['email']
			);
	
	// checking empty fields
	$errorMessage = '';
	foreach ($user as $key => $value) {
		if (empty($value)) {
			$errorMessage .= $key . ' field is empty<br />';
		}
	}
			
	if ($errorMessage) {
		// print error message & link to the previous page
		echo '<span style="color:red">'.$errorMessage.'</span>';
		echo "<br/><a href='javascript:self.history.back();'>Ir atrás</a>";	
	} else {
		//updating the 'users' table/collection
		$db->users->update(
						array('_id' => new MongoId($id)),
						array('$set' => $user)
					);
		
		//redirectig to the display page. In our case, it is index.php
		header("Location: index.php");
	}
} // end if $_POST
?>
<?php
//getting id from url
$id = $_GET['id'];

//selecting data associated with this particular id
$result = $db->users->findOne(array('_id' => new MongoId($id)));

$nombre = $result['nombre'];
$edad = $result['edad'];
$email = $result['email'];
?>
<html>
<head>	
	<title>Edición de Usuario</title>
</head>

<body>
	<a href="index.php">Home</a>
	<br/><br/>
	
	<form name="form1" method="post" action="edit.php">
		<table border="0">
			<tr> 
				<td>Nombre</td>
				<td><input type="text" name="nombre" value="<?php echo $nombre;?>"></td>
			</tr>
			<tr> 
				<td>Edad</td>
				<td><input type="text" name="edad" value="<?php echo $edad;?>"></td>
			</tr>
			<tr> 
				<td>Email</td>
				<td><input type="text" name="email" value="<?php echo $email;?>"></td>
			</tr>
			<tr>
				<td><input type="hidden" name="id" value=<?php echo $_GET['id'];?>></td>
				<td><input type="submit" name="update" value="Actualizar"></td>
			</tr>
		</table>
	</form>
</body>
</html>
