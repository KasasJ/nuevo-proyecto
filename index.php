<?php
//including the database connection file
include ("config.php");

// select data in descending order from table/collection "users"
$result = $db->users->find()->sort(array('_id' => -1));
?>

<html>
<head>	
	<title>Homepage</title>
</head>

<body>
<a href="add.html">Agregar Usuario</a><br/><br/>

	<table width='80%' border=0>

	<tr bgcolor='#CCCCCC'>
		<td>Nombre</td>
		<td>Edad</td>
		<td>Correo Electrónico</td>
		<td>Actualizar</td>
	</tr>
	<?php 	
	foreach ($result as $res) {
		echo "<tr>";
		echo "<td>".$res['nombre']."</td>";
		echo "<td>".$res['edad']."</td>";
		echo "<td>".$res['email']."</td>";	
		echo "<td><a href=\"edit.php?id=$res[_id]\">Editar</a> | <a href=\"delete.php?id=$res[_id]\" onClick=\"return confirm('Está seguro de Eliminar este usuario?')\">Eliminar</a></td>";		
	}
	?>
	</table>
</body>
</html>
