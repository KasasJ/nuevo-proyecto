<html>
<head>
	<title>Agregando Usuarios</title>
</head>

<body>
<?php
//including the database connection file
include ("config.php");

if(isset($_POST['Submit'])) {	
	$user = array (
				'nombre' => $_POST['nombre'],
				'edad' => $_POST['edad'],
				'email' => $_POST['email']
			);
		
	// checking empty fields
	$errorMessage = '';
	foreach ($user as $key => $value) {
		if (empty($value)) {
			$errorMessage .= 'el campo '.$key . ' está vacío<br />';
		}
	}
	
	if ($errorMessage) {
		// print error message & link to the previous page
		echo '<span style="color:red">'.$errorMessage.'</span>';
		echo "<br/><a href='javascript:self.history.back();'>Ir atrás</a>";	
	} else {
		//insert data to database table/collection named 'users'
		$db->users->insert($user);
		
		//display success message
		echo "<font color='green'>Usuario Agregado correctamente.";
		echo "<br/><a href='index.php'>Ver Lista de Usuarios</a>";
	}
}
?>
</body>
</html>
